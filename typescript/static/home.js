import {loadLayout} from './LoadLayout.js';

const pzElement = document.getElementById('scene');
const pz_scene = panzoom(pzElement);

$.get({
    type: 'GET',
    datatype: 'text',
    url: '/track_layout',
    success: (data) => {
        console.log(loadLayout(JSON.stringify(data)));
    }
});