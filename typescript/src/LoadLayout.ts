import {Track, connectTwo} from "./TrackConstructors.js";
import * as bluePrints from "./TrackBluePrints.js";

// Dirty but it works
type bluePrintsDict = {[trackType: string]: new() => Track};
const bp = {...bluePrints} as bluePrintsDict;

export function loadLayout(jsonString: string): {[name: string]: Track} {
    const jsonLayout = JSON.parse(jsonString);
    const tracks: {[trackName: string]: Track} = {};

    for (const trackName of Object.keys(jsonLayout.createTracks)) {
        const trackType = jsonLayout.createTracks[trackName];
        tracks[trackName] = new bp[trackType]();
    }
    for (const [track0Name, port0, track1Name, port1] of jsonLayout.connectTracks) {
        connectTwo(tracks[track0Name], port0, tracks[track1Name], port1);
    }

    return tracks;
}