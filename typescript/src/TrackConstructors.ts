// import * as math from "mathjs";
// import * as Color from "color";
import {createSVGElement} from "./CommonFunctions";


export interface Track {Commons
    getPorts(): string[];
    connect(otherTrack: Track, port: string): void;
    getNextTrack(fromPort: string): Track;
    getOtherPort(port: string): string;
    // draw(refPort: string, refLocation: math.Matrix, refAngle:number, color: Color): [math.Matrix, number][]
}

class Port {
    name: string;
    track: Track = undefined;

    constructor(name: string) {
        this.name = name;
    }
}

export function constructRail(length?: number, angle?: number, radius: number = 0): new(port0?: string, port1?: string, shapeless?: boolean) => Track {
    class Rail implements Track{
        port0: Port;
        port1: Port;
        straight: boolean;
        length: number;
        angle: number;
        radius: number;
        shapeless: boolean;
        blocker: boolean;

        constructor(port0: string = '0', port1: string = '1', shapeless: boolean = false) {
            this.port0 = new Port(port0);
            this.port1 = new Port(port1);

            this.angle = angle;
            this.radius = radius;
            this.straight = radius === 0;
            this.length = length;
            if (this.straight) {
                this.length = length;
            } else {
                this.length = 2 * Math.PI * radius * angle / 360;
            }
            this.shapeless = shapeless;
            this.blocker = port1 === undefined;
        }

        getPorts(): string[] {
            return [this.port0.name, this.port1.name];
        }

        connect(otherTrack: Track, port: string): void {
            if (this.getPorts().includes(port)) {
                if (port === this.port0.name) {
                    this.port0.track = otherTrack;
                } else {
                    this.port1.track = otherTrack;
                }
            } else {
                throw new NonExistentPort(this, port)
            }
        }

        getNextTrack(fromPort: string): Track {
            if (fromPort === this.port0.name) {
                return this.port1.track;
            } else {
                return this.port0.track;
            }
        }

        getOtherPort(port: string): string {
            if (port === this.port0.name) {
                if (this.blocker) {
                    return undefined;
                } else {
                    return this.port1.name;
                }
            } else if (port === this.port1.name) {
                return this.port0.name;
            } else {
                throw new NonExistentPort(this, port);
            }
        }

        // draw(refPort: string, refLocation: math.Matrix, refAngle: number, color: Color): [math.Matrix, number][] {
        //     return undefined
        // }
    }

    return Rail;
}

export function constructMultiRail(railsOnStates: {[state: string]: Track[]}): new(initialState?: string) => Track {
    class MultiRail implements Track {
        rails: {[state: string]: Track[]};
        state: string;

        constructor(initialState: string = '0') {
            this.rails = railsOnStates;
            this.state = initialState;
        }

        getAllRails(): Track[] {
            return [].concat(...Object.values(this.rails));
        }

        getPorts(): string[] {
            return [...new Set([].concat(...this.getAllRails().map(r => r.getPorts())))];
        }

        connect(otherTrack: Track, port: string): void {
            if (this.getPorts().includes(port)) {
                for (const rail of this.getAllRails()) {
                    if (rail.getPorts().includes(port)) {
                        rail.connect(otherTrack, port);
                    }
                }
            } else {
                throw new NonExistentPort(this, port);
            }
        }

        getActiveRail(fromPort: string): Track {
            for (const rail of this.rails[this.state]) {
                if (rail.getPorts().includes(fromPort)) {
                    return rail;
                }
            }
            throw new NonActivePort(this, fromPort, this.state);
        }

        getNextTrack(fromPort: string): Track {
            return this.getActiveRail(fromPort).getNextTrack(fromPort);
        }

        getOtherPort(port: string): string {
            return this.getActiveRail(port).getOtherPort(port);
        }

        // draw(refPort: string, refLocation: math.Matrix, refAngle: number, color: Color): [math.Matrix, number][] {
        //     return undefined
        // }
    }

    return MultiRail
}

export function connectTwo(track0: Track, port0: string, track1: Track, port1: string): void {
    track0.connect(track1, port0);
    track1.connect(track0, port1);
}

class NonExistentPort extends Error {
    constructor(track: Track, port: string) {
        super("Tried to access port "+port+" of "+track+" but it doesn't have this port");
    }
}

class NonActivePort extends Error {
    constructor(track: Track, port: string, state: string) {
        super(track+"doesn't have port "+port+" active in state "+state);
    }
}
