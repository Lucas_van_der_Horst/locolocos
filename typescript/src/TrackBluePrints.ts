import {constructRail, constructMultiRail} from './TrackConstructors.js'

export const f9101 = constructRail(111);
export const f9136 = constructRail(undefined, 15, 430);
export const f9173 = constructMultiRail({0: [new f9101('0', '1')], 1: [new f9136('0', '2')]})
