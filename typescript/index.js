var express = require('express');
const dotenv = require( "dotenv");
const path = require( "path");
const ll = require('./dist.node/LoadLayout')

// initialize configuration
dotenv.config();

// port is now available to the Node.js runtime
// as if it were an environment variable
const port = process.env.SERVER_PORT;

const app = express();
const layoutFile = "example.json";

//app.use('/static', express.static(__dirname+'/static'))
app.use(express.static(__dirname + '/static'));
app.use(express.static(__dirname + '/dist.browser'));

// define a route handler for the default home page
app.get("/", (req, res ) => {
    res.sendFile(path.join(__dirname + '/static/home.html'));
});

app.get("/track_layout", (req, res) => {
    res.sendFile(path.join(__dirname + '/track_layouts/' + layoutFile))
});

// start the Express server
app.listen( port, () => {
    // tslint:disable-next-line:no-console
    console.log( `server started at http://localhost:${ port }` );
});